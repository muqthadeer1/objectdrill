//define a function with object as arguement
function key(object) {
  // initialize the empty array to store the result
  let keyArr = [];
  for (let key in object) {
    keyArr.push(key);
  }
  return keyArr;
}

// create a module to exports the function to other file
module.exports = key;
