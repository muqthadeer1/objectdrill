//define the function with object as arguement
function values(object) {
  //initialize the empty array
  let arrValues = [];
  for (let key in object) {
    arrValues.push(object[key]);
  }
  return arrValues;
}

//create a module to export the function to other directories
module.exports = values;
