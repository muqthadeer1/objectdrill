//define the function mapObject with two object and callBack function as arguements
function mapObject(object, cb) {
  // initialize the empty object
  let changeObject = {};
  {
    for (let key in object) {
      changeObject[key] = cb(object[key]);
    }
  }
  return changeObject;
}

//create a module to exports the function to other file
module.exports = mapObject;
