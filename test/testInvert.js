//create a variable to imports the function from other directory
let fnInvert = require("../invert.js");

//create a variable to execute the function which is imported.
let invertObject = fnInvert({ a: 1, b: 2, c: 3 });

//it prints the output
console.log(invertObject);
 