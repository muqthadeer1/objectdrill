// create a variable to import the function from other directory
let fnKey = require("../key.js");

// create a variable and Call the function with an object as an argument
let findPropetyOfObject = fnKey({
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham", 
});

// it prints output
console.log(findPropetyOfObject);
