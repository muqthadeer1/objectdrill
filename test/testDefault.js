// create a variable to import the function from other directory
let fnDefault = require("../default.js");

//define a variable object with undefined values
let objectValue = { a: undefined, b: undefined };

//Define the object this object used as the defaults value
let defaultValue = { a: 1, b: 2, c: 3 };

//create the variable to store the data which is executed by function
let updatedObject = fnDefault(objectValue, defaultValue);

// printthe output
console.log(updatedObject);
 