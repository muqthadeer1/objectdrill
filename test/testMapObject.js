//create a variable to execute the function from other directory
let fnMapObjects = require("../mapObject.js");

// Define the function with one arguement
function cb(value) {
  return value * value;
}

// define a object
let object = { a: 2, b: 3, c: 4 };

//create a variable and call the function with object and callBack functions as arguements
let finalObject = fnMapObjects(object, cb);

// it prints final Output
console.log(finalObject, object);
 