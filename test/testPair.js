// create a variable to import the function from the other directory
let fnPair = require("../pair.js");

//create variable and call the function with object as arguement
let pairObject = fnPair({ a: 1, b: 2, c: 3 });

// it prints the output
console.log(pairObject);
  