// create a variable to import the function from the other directory
let fnValue = require("../values.js");

//create a variable  and call the function with object as arguement
let findValue = fnValue({ name: "Bruce Wayne", age: 36, location: "Gotham" });

//it prints the output
console.log(findValue);
  