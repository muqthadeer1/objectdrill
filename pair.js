//define the function with object as arguement
function pairs(object) {
  return Object.entries(object);
}

// create a module to export the function to other directories
module.exports = pairs;
