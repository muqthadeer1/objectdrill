// define a function with object as arguement
function invert(object) {
  // initialize object with empty
  let finalObject = {};

  for (let key in object) {
    let value = object[key];
    finalObject[value] = key;
  }
  return finalObject;
}

// create a module to exports the function to other directory
module.exports = invert;
