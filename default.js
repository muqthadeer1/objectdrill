// define the function with two arguements
function defaultObjectfn(object, defaultObject) {
  for (let key in defaultObject) {
    // if the object value is undefined
    if (object[key] == undefined) {
      object[key] = defaultObject[key];
    }
  }
  return object;
}

//create a module to export the function from other file
module.exports = defaultObjectfn;
